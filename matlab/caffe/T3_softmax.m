function mu = T3_softmax(eta)
    % Softmax function for 2 dim
    eeta = exp(eta);
    deta = repmat(sum(eeta,1),[size(eta,1),1]);
    mu = eeta./deta;
end
