function [input,ex] = processIm_lfw(img, parm, shape)

%% ---- GeneratePrior.m ----- %%
ex = single(255 .* GeneratePrior(shape));

input = cat(3,img,ex);

% padding 252 -> 324, 16 images

%% ---- T6_EPsharePadding.m ----- %%
input = T6_EPsharePadding(input,parm);

% input preprocessing
for kk = 1:6
    input(:,:,kk,:) = single(input(:,:,kk,:)) - parm.mean(kk);
end
% permute from RGB to BGR
input = input(:,:,[3 2 1 4:end],:);
% switch width with height
input = permute(input,[2 1 3 4]);
