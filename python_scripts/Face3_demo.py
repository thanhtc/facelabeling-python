import os
import caffe
import numpy as np

from skimage.io import imread

from Face3Init_01 import Face3Init_01
from Face3Classes import Face3Classes
from common import load


solver = 'lfw03_solver_exemplar'
proto_path = '../examples/LFW_cvpr15/'
solver_file = os.path.join(proto_path, solver + '.prototxt')
solver_mat = os.path.join(proto_path, solver + '.mat')

# MATLAB: if caffe('is_initialized') ~= 2
solver = Face3Init_01(solver_file, solver_mat)

if os.path.exists('resources/LFW_EP_EX_MEAN.mat'):
    LFW_EP_MEAN = load('resources/LFW_EP_EX_MEAN.mat', ['LFW_EP_MEAN'])[0][0]
else:
    LFW_EP_MEAN = 100 * np.ones((1, 6))

parm = {}
parm['patch_size'] = 72
parm['mini_batch'] = 1
parm['imsize'] = 250
parm['amp'] = 100
parm['mean'] = LFW_EP_MEAN
parm['result_path'] = 'Results'
if not os.path.exists(parm['result_path']):
    os.makedirs(parm['result_path'])

img = imread('resources/img2.png')
shape = []
with open('resources/shape2.txt') as f:
    for line in f:
        x, y = line.split(' ')
        shape.append([int(x), int(y)])
shape = np.array(shape)

lab = Face3Classes(img, shape, parm)
save(os.path.join(parm['result_path'], 'lab.mat'), {'lab': lab})
